CONTAINER_NAME ?= neovim
UID ?= $(shell id -u)
GUID ?= $(shell id -g)
CWD ?= $(shell pwd)
CONTAINER_HOST ?= $(shell which podman > /dev/null && which podman || which docker)
NEOVIM_VERSION ?= v0.5.1

phoney: build run

update:
	cd neovim && \
		git checkout master && \
		git pull && \
		git checkout tags/v0.5.0
	${CONTAINER_HOST} image rm ${CONTAINER_NAME}

pull:
	@if [ ! -d neovim ]; then \
		git clone https://github.com/neovim/neovim; \
		cd neovim && git checkout master && git pull && git checkout tags/v0.5.0; \
	fi
	@if [ ! -d rust-analyzer ]; then \
		git clone https://github.com/rust-analyzer/rust-analyzer.git; \
	fi
	@if [ ! -d tig ]; then \
		git clone https://github.com/jonas/tig; \
	fi
	@cd neovim && \
		git checkout master && \
		git pull && \
		git checkout tags/${NEOVIM_VERSION}
	@cd rust-analyzer && \
		git checkout master && \
		git pull;
	@cd tig && git checkout master && git pull

build:
	${CONTAINER_HOST} build -t ${CONTAINER_NAME} \
		--build-arg UID=${UID} \
		--build-arg GUID=${GUID} \
		-f docker/Dockerfile .

install:
	@if [ ! -d "${HOME}/.local/bin" ]; then \
		echo "Creating ${HOME}/.local/bin"; \
		mkdir -p "${HOME}/.local/bin"; \
	fi
	@cp nv ${HOME}

run:
	${CONTAINER_HOST} volume create neovim_roaming_home || true
	${CONTAINER_HOST} run \
		--rm \
		-v ${CWD}:/workspace \
		-v neovim_roaming_home:/home/neovim_user \
		-it ${CONTAINER_NAME}

