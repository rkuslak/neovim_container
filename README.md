NeoVIM Containers
===
A set of scripts and programs to create containers to run NeoVIM with expected
language servers provided within the container.

To use it you will likely want to run `make pull` first to ensure the source
repos used by the containers are available.  You can then build the image as
`neovim` by running `make build`. To then start a shell using the image, run
`make` from the repo root; it will mount the repo as /workspace, and the user
home directory accordingly. You can then copy any desired configuration files
using `docker container cp` so they are available when called.

Currently most of the orchestration is handled by the python script `nv`;
ultimately a lot of the work I want for this was already done to get
`multibuilder` working as I intended or is in a similar enough state to have a
good deal of spill over, so I will likely end up migrating that work into it
eventually.

The idea behind using this is that:
* The user will invoke `nv` from within a project they are working on
* A persistent mount for the user's home directory is mapped, so command
  history and config files are retained between uses
* The assumed root of the project is loaded as the `/workspace` mount in the
  container, and if a file is passed it is made relative to the project root
  and passed as a file to open to NeoVIM when the container starts
