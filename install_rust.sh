if ! [ -x "$(command -v rustc)" ]; then
    curl https://sh.rustup.rs -sSf | bash -s -- -y
    export PATH="${HOME}/.cargo/bin:${PATH}"
else
    rustup update
fi

if [ -d rust-analyzer ]; then
    git clone https://github.com/rust-analyzer/rust-analyzer.git
end

cd rust-analyzer && \
    git checkout master && \
    git pull && \
    cargo xtask install --server
