#!/usr/bin/env python3

import argparse
import pathlib
import shlex
import shutil
import subprocess
import sys

HOME_VOLUME_NAME = "neovim_roaming_home"
CONTAINER_NAME = "neovim"
USER_NAME = "neovim_user"

# If podman is available, default to using it.
if shutil.which("podman"):
    CONTAINER_HOST = "podman"
else:
    CONTAINER_HOST = "docker"


def ensure_home_volume_exists():
    """
    Attempts to validate that the expected volume for the persisted user home
    cache is available.
    """
    try:
        inspect_cmd = [CONTAINER_HOST, "volume", "inspect", HOME_VOLUME_NAME]
        subprocess.run(inspect_cmd, check=True, capture_output=True)
    except BaseException:
        # Attempting to inspect the volume caused Docker to throw a error;
        # attempt to create the volume as we believe it doesn't exist
        subprocess.call([CONTAINER_HOST, "volume", "create", HOME_VOLUME_NAME])


def get_path(original_path: pathlib.Path, project_root: pathlib.Path) -> pathlib.Path:
    """
    Provided with a path relative to the current directory, finds the project
    root for the current project (if possible), and returns a tuple of the
    project root to start from, with a relative-to-root path to the file.
    """
    project_relative_path = pathlib.Path(original_path).resolve().absolute()

    return project_relative_path.relative_to(project_root)


def get_project_root_path(starting_path: pathlib.Path) -> pathlib.Path:
    """
    Provided with a path believed to be a subpath of a project, attempts to
    find the project root from the given path.
    """
    project_root_path = pathlib.Path(starting_path).resolve().absolute()
    potential_root_paths = []
    if project_root_path.is_dir():
        potential_root_paths.append(project_root_path)
    potential_root_paths.extend(project_root_path.parents)

    for current_path in potential_root_paths:
        # TODO: For now we're going to assume if the directory has a
        # subdirectory ".git", it is a project root. We need to be a bit
        # smarter about this.
        gitable_path = current_path / ".git"
        if gitable_path.exists() and gitable_path.is_dir():
            project_root_path = current_path
            break

    return project_root_path


def main():
    # If we get one argument, assume we're working relative to the current
    # directory; otherwise the first path is the expected project root, and
    # every file after is a file to edit. If we get nothing, assume we just
    # want to start from the project root without anything loaded initially.
    parser = argparse.ArgumentParser()
    parser.add_argument("--cmd")
    parser.add_argument("--workdir", "--work", "--cwd")
    parser.add_argument("files", nargs="*")
    options = parser.parse_args()

    file_paths = [pathlib.Path(p) for p in options.files]
    if options.workdir:
        root_path = pathlib.Path(options.workdir)
    else:
        root_path = pathlib.Path()
    root_path = root_path.absolute()

    root_path = get_project_root_path(root_path)
    file_paths = [get_path(p, root_path) for p in file_paths]

    if options.cmd:
        quoted_command = options.cmd
    elif file_paths:
        quoted_command_builder = ["nvim", *[str(p) for p in file_paths]]
        quoted_command = shlex.join(quoted_command_builder)
    else:
        quoted_command = "nvim"

    ensure_home_volume_exists()

    container_command = [
        CONTAINER_HOST,
        "run",
        "--rm",
        "-v",
        f"{HOME_VOLUME_NAME}:/home/{USER_NAME}",
        "-v",
        f"{root_path}:/workspace",
        "-it",
        CONTAINER_NAME,
        "/bin/sh",
        "-c",
        quoted_command,
    ]
    subprocess.run(container_command)


if __name__ == "__main__":
    main()

